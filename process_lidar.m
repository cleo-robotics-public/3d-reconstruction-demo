clear;

location_sensor_data = "log0000/run/mpa/vvpx4_body_wrt_local";
location_pc_data = "log0001/run/mpa/tof";

%% Load orientation & position
imuOrientations_raw = readtable(location_sensor_data + '/data.csv');
inertialtimes = [];
orientations = [];
positions = [];
for i = 1:size(imuOrientations_raw,1)

	% Time
	ns = uint64(imuOrientations_raw{i,2});
	wholeSecs = floor(double(ns)/1e9);
	fracSecs = double(ns - uint64(wholeSecs)*1e9)/1e9;
	t = datetime(wholeSecs,'ConvertFrom','posixTime','Format','yyyy.MM.dd HH:mm:ss.SSSSSSSSS') + seconds(fracSecs);
	inertialtimes = [inertialtimes; ns];
	
	% Orientation
	pitch = imuOrientations_raw{i,7};
	roll = imuOrientations_raw{i,6};
	yaw = imuOrientations_raw{i,8};
	orientations = [orientations; double(pitch) double(roll) double(yaw)];

	% Position
	x = imuOrientations_raw{i,3};
	y = imuOrientations_raw{i,4};
	z = imuOrientations_raw{i,5};
	positions = [positions; x y z];
end
imuOrientations = [inertialtimes, orientations];
positionData = [inertialtimes, positions];

%% Lidar
times = [];
pointclouds = [];
lidar_raw = readtable(location_pc_data + '/data.csv');
for i = 1:size(lidar_raw,1)
	filename = location_pc_data + "/data_" + lidar_raw{i,1} + ".pcd";

	ns = uint64(lidar_raw{i,2});
	wholeSecs = floor(double(ns)/1e9);
	fracSecs = double(ns - uint64(wholeSecs)*1e9)/1e9;
	t = datetime(wholeSecs,'ConvertFrom','posixTime','Format','yyyy.MM.dd HH:mm:ss.SSSSSSSSS') + seconds(fracSecs);
	times = [times; ns];

	pc = pcread(filename);
	pointclouds = [pointclouds; pc];
end
lidartimes = times;
lidardata = pointclouds;

%% Visualization
tic;

ind1 = 1;
ind2 = length(lidardata) - 1;

flightime = (lidartimes(ind2) - lidartimes(ind1)) / 1e9;
flightime_m = floor(double(flightime) / 60);
flightime_s = mod(flightime, 60);

clearvars map;

for i = ind1:ind2
	disp(i+"/"+ind2);

	pc = lidardata(i);
	
	% Find closest position timestamp index - source is the same so timestamps are the same for both data sets
	ind2_closest = 0;
	ind2_minimum = 1000000000;
	for k = 1:length(inertialtimes)
		diff2 = abs(int64(inertialtimes(k)) - int64(lidartimes(i)));
		if diff2 < ind2_minimum
			ind2_minimum = diff2;
			ind2_closest = k;
		end
	end
	pos2 = [positions(ind2_closest, 1) positions(ind2_closest, 2) positions(ind2_closest, 3)];
	pos2 = double(pos2);	
	
	eul2 = [orientations(ind2_closest, 2) orientations(ind2_closest, 1) orientations(ind2_closest, 3)];
	eul2 = double(eul2);
	rot2 = eul2rotm(eul2, "XYZ");

	tform2 = rigid3d(rot2, pos2);
	
	%pc = pcdownsample(pc, 'gridAverage', 0.5);
	pc_t = pctransform(pc, tform2);

	if exist('map', 'var') == 1
		map = pcmerge(map, pc_t, 0.01);
	else
		map = pc_t
	end
end

toc;

figure;
pcshow(map);
xlabel('X');
ylabel('Y');
zlabel('Z');

disp("flight time: " + flightime_m + " minutes and " + flightime_s + " seconds");

pcwrite(map, 'map.pcd');
