# 3D Reconstruction Demo

This repository contains an example of point cloud collection and subsequent stitching in Matlab. It uses telemetry data from the Dronut's positioning system to align each frame and stitch it together into a single point cloud.

## Prerequisites

The Lidar needs to be enabled in order to collect point cloud data. To do this set the enabled flag to true under port J4 in `/etc/modalai/voxl-camera-server.conf` then synchronize file system changes with the `sync` command and reboot the Dronut.

## Steps

1. Upload the updated `voxl-logger` binary to the Dronut along with the `start.sh` and `stop.sh` scripts
2. To start data collection run the `start.sh` script on the Dronut
3. To stop data collection run the `stop.sh` script on the Dronut
4. Download the collected data in the `/data/voxl-logger` folder from the Dronut
5. Update the folder name and location at the top of the `process_lidar.m` script
6. Run the `process_lidar.m` script in Matlab to align and stitch the point cloud frames into a single cloud

## License

MIT